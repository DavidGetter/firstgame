package sample;

import javafx.scene.input.KeyCode;
import sample.model.Model;

public class InputHandler {

    private Model model;

    public InputHandler(Model model){
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode){

        if(keycode == KeyCode.RIGHT){
            model.getPlayer().move(15,0);
        }else if(keycode == KeyCode.LEFT){
            model.getPlayer().move(-15,0);
        }
    }




}
