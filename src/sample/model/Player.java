package sample.model;

public class Player {

    private double x;
    private double y;
    private double h;
    private double w;

    Player(double x, double y) {
        this.x = x;
        this.y = y;
        this.h = 30;
        this.w = 30;
    }

    public void move(double dx, double dy){
        if(this.x + dx < 990 && this.x + dx > 0) {
            this.x += dx;
            this.y += dy;
        }
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getH() {
        return h;
    }

    public double getW() {
        return w;
    }






}
