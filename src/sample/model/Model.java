package sample.model;

import java.util.LinkedList;
import java.util.List;

public class Model {

    private Player player;
    private List<Stopper> stoppers = new LinkedList<>();
    private List<Stopper> stoppersRemove = new LinkedList<>();
    private long stopperRebuild;
    private int stopperLimit;
    private boolean gameOver = false;
    private boolean gameWon = false;
    private int score = 0;

    public final double WIDTH = 990;
    public final double HEIGHT = 770;

    public Model() {
        // Player
        this.player = new Player(495, 730);

        this.stopperRebuild = 1000;
        this.stopperLimit = 1000;

        // Stopper
        stoppers.add(new Stopper());
        boolean stopperFit;
        for (int i = 0; i < 5; ) {
            Stopper stopper = new Stopper();
            stopperFit = true;
            for (Stopper stopper1 : stoppers) {
                if (!(Math.abs(stopper.getX() - stopper1.getX()) > 60 &&
                        Math.abs(stopper.getY() - stopper1.getY()) > 30)) {
                    stopperFit = false;
                    break;
                }
            }
            if (stopperFit) {
                i++;
                stoppers.add(stopper);
            }

        }

    }


    public List<Stopper> getStoppers() {
        return stoppers;
    }

    public Player getPlayer() {
        return player;
    }

    public void update() {
        player.move(0, -0.6);
        if (checkCollisionStopper(player)) {
            player.move(0, 0.6);
        }

        for (Stopper stopper : stoppers) {

            if (stopper.getY() > HEIGHT) {
                score += 1;
                stoppersRemove.add(stopper);
            }
        }
        stoppers.removeAll(stoppersRemove);


        if (player.getY() > 760) {
            player.move(0, 0.6);
            for (Stopper stopper : stoppers) {
                stopper.moveStopper(0, -0.5);
            }
            gameOver = true;
        }

        if (player.getY() < 10) {

            player.move(0, 0.6);
            for (Stopper stopper : stoppers) {
                stopper.moveStopper(0, -0.5);
            }
            gameWon = true;
        }
    }

    public void gameWin() {
        if (player.getY() < 10) {
            player.move(0, 0.6);
            for (Stopper stopper : stoppers) {
                stopper.moveStopper(0, -0.5);
            }
            gameWon = true;
        }
    }

    public void gameOverLost() {
        if (player.getY() > 760) {
            player.move(0, 0.6);
            for (Stopper stopper : stoppers) {
                stopper.moveStopper(0, -0.5);
            }
            gameOver = true;
        }
    }

    public void updateStopper(long time) {
        if(!gameWon && !gameOver) {
            if (stopperRebuild >= 1000) {

                stoppers.add(new Stopper());
                stopperRebuild = 0;
            }
        }
        stopperRebuild += time;

        for (Stopper stopper1 : stoppers) {
            stopper1.moveStopper(0, 0.5);
        }
        if (checkCollisionStopper(player)) {
            player.move(0, 0.5);
        }

    }


    public void updateFasterStopper(long time) {
        if(!gameWon && !gameOver) {
            if (stopperRebuild >= stopperLimit) {
                if (stopperLimit > 200) {
                    stopperLimit -= 75;
                }
                stoppers.add(new Stopper());
                stopperRebuild = 0;
            }
        }
        stopperRebuild += time;

        for (Stopper stopper1 : stoppers) {
            stopper1.moveStopper(0, 0.5);
        }
        if (checkCollisionStopper(player)) {
            player.move(0, 0.5);
        }

    }


    private boolean checkCollisionStopper(Player player) {
        for (Stopper stopperS : stoppers) {
            if (checkCollision(player, stopperS)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkCollision(Player player, Stopper stopper) {
        double dx = Math.abs(player.getX() - stopper.getX());
        double dy = Math.abs(player.getY() - stopper.getY());
        double w = player.getW() / 2 + stopper.getW() / 2;
        double h = player.getH() / 2 + stopper.getH() / 2;
        if (dx <= w && dy <= h) {
            return true;
        }
        return false;
    }

    public int getScore() {
        return this.score;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public boolean isGameWon() {
        return gameWon;
    }
}
