package sample.model;

public class Stopper {

        private double x;
        private double y;
        private int h;
        private int w;

        Stopper() {
            this.x = Math.random()*990;
            this.y = Math.random()*300;
            this.h = 10;
            this.w = 100;
        }

    public void moveStopper(double dx, double dy){
        if(this.x + dx < 990 && this.x + dx > 0) {
            this.x += dx;
            this.y += dy;
        }
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }
}
