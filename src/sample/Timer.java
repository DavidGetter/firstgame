package sample;

import javafx.animation.AnimationTimer;
import sample.model.Model;

public class Timer extends AnimationTimer {

    private Graphics graphics;
    private Model model;
    private long previousTime = -1;
    private long totalTime = 0;

    public Timer(Graphics graphics, Model model){
        this.graphics = graphics;
        this.model = model;
    }



    @Override
    public void handle(long nowNano) {
        model.update();

        graphics.drawPlayer();

        long nowMillis = nowNano / 1000000;
        long elapsedTime;
        if (previousTime == -1) {
            elapsedTime = 0;
        } else {
            elapsedTime = nowMillis - previousTime;
        }
        previousTime = nowMillis;


        totalTime += elapsedTime;

        if(totalTime < 1500) {
            model.updateStopper(elapsedTime);
            graphics.drawObstacle();
            model.gameWin();
            model.gameOverLost();

        }else {
            model.updateFasterStopper(elapsedTime);
            graphics.drawObstacle();
            model.gameWin();
            model.gameOverLost();
        }
    }



}
