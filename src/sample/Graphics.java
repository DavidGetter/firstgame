package sample;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import sample.model.Model;
import sample.model.Stopper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Graphics {

    private GraphicsContext gc;
    private Model model;

    private Image backGround = new Image(new FileInputStream("src/images/sky.png"));
    private Image obstacle = new Image(new FileInputStream("src/images/obstacle.png"));
    private Image smiley = new Image(new FileInputStream("src/images/smile.png"));
    private Image whiteSmile = new Image(new FileInputStream("src/images/whiteAndSmiley.png"));


    // Image backGround = new Image("src/images/heaven.png", 990, 770, true, true);

    public Graphics(GraphicsContext gc, Model model) throws FileNotFoundException {
        this.gc = gc;
        this.model = model;
    }


    void drawPlayer(){

        gc.clearRect(0,0,model.WIDTH,model.HEIGHT);

        gc.drawImage(backGround, 0, 0);


        gc.drawImage(smiley, model.getPlayer().getX()-15, model.getPlayer().getY()-15,
                model.getPlayer().getW(), model.getPlayer().getH()
        );

        if(model.isGameOver() == true){
            gc.drawImage(whiteSmile,0,0,990,770);
            gc.setStroke(Color.BLACK);
            gc.setFont(Font.font(100));
            gc.fillText("You lost " + "\n" + "Score does not count :   " + model.getScore(), 200 , 350 , 500);

        }

        if(model.isGameWon() == true){
            gc.drawImage(whiteSmile,0,0,990,770);
            gc.setStroke(Color.BLACK);
            gc.setFont(Font.font(100));
            gc.fillText("You Won - Congratulations! " + "\n" + "Your Score :   " + model.getScore(), 200 , 350 , 500);
        }

    }

    void drawObstacle() {
        if(!model.isGameOver() && !model.isGameWon()) {
            for (Stopper stopper : model.getStoppers()) {
                gc.drawImage(obstacle, stopper.getX() - 50, stopper.getY() - 5, stopper.getW(), stopper.getH());
            }

            gc.setStroke(Color.BLACK);
            gc.setFont(Font.font(40));
            gc.strokeText("Score " + model.getScore(), 0, 50, 100);

            gc.setStroke(Color.BLACK);
            gc.setFont(Font.font(40));
            gc.strokeText("Move to the top! ", 790, 50, 200);
        }


    }


}
